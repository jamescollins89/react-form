import React from 'react';
import './App.css';
import { Container } from 'semantic-ui-react';
import FormBuild from './Components/Form/FormBuild';

function App() {
  return (
    <div className="App">
      <Container>
        <FormBuild />
      </Container>
    </div>
  );
}

export default App;
