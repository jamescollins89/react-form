import React, { Component } from 'react';
import FormUserDetails from './FormUserDetails';
import FormProductSelection from './FormProductSelection';
import FormProductOptions from './FormProductOptions';
import Error from './Error';

class FormBuild extends Component {
    state = {
        step: 1,
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        addressLine1: '',
        addressLine2: '',
        postcode: '',
        productOne: {
            selected: false,
            title: 'Computer',
            price: 999
        },
        productTwo: {
            selected: false,
            title: 'Laptop',
            price: 499
        }

    };

    // Proceed to Next Step
    nextStep = () => {
        const { step } = this.state;
        this.setState({
            step: step + 1
        });
    };

    // Return to Previous Step
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    };

    // Handle input change
    handleChange = (e) => {
        if (e.target.type === 'checkbox') {
            this.setState({
                [e.target.name]: {
                    ...this.state[e.target.name],
                    selected: e.target.checked
                }
            })
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }

    handleGetState = () => {
        console.log(this.state);
    }

    render() {
        const { step } = this.state;
        const { firstName, lastName, email, phoneNumber, addressLine1, addressLine2, postcode, productOne, productTwo } = this.state;
        const values = { firstName, lastName, email, phoneNumber, addressLine1, addressLine2, postcode, productOne, productTwo };

        switch (step) {
            case 1:
            return (
                <FormUserDetails
                    nextStep={this.nextStep}
                    handleChange={this.handleChange}
                    handleGetState={this.handleGetState}
                    values={values}
                />
            );
            case 2:
            return (
                <FormProductSelection
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange={this.handleChange}
                    handleGetState={this.handleGetState}
                    values={values}
                />
            );
            case 3:
                return (
                    <FormProductOptions
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        handleGetState={this.handleGetState}
                        values={values}
                    />
                );
            default:
                return (
                    <Error />
                );
        }
    }
}
 
export default FormBuild;