import React, { Component } from 'react';
import { Input, Grid, Button } from 'semantic-ui-react';

class FormProductSelection extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const { values, handleChange, handleGetState } = this.props;

        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column><h1>Please Select a Product</h1></Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column><Input type="checkbox" name="productOne" checked={values.productOne.selected} onChange={handleChange} /> {values.productOne.title}</Grid.Column>
                    <Grid.Column><Input type="checkbox" name="productTwo" checked={values.productTwo.selected} onChange={handleChange} /> {values.productTwo.title}</Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column><Button fluid onClick={handleGetState}>GetState</Button></Grid.Column>
                    <Grid.Column><Button fluid onClick={this.back}>Back</Button></Grid.Column>
                    <Grid.Column><Button fluid onClick={this.continue}>Continue</Button></Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
 
export default FormProductSelection;