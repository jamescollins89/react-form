import React, { Component } from 'react';
import { Grid, Input, Button } from 'semantic-ui-react';

class FormUserDetails extends Component {
    continue = e => {
        e.preventDefault();
        console.log(this.props.values);
        this.props.nextStep();
    };

    render() {
        const { values, handleChange, handleGetState } = this.props;
        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column><h1>Please Enter Your Details</h1></Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column><Input fluid placeholder='First Name' name="firstName" defaultValue={values.firstName} onChange={handleChange} /></Grid.Column>
                    <Grid.Column><Input fluid placeholder='Last Name' name="lastName" defaultValue={values.lastName} onChange={handleChange} /></Grid.Column>
                    <Grid.Column><Input fluid placeholder='Email Address' name="email" defaultValue={values.email} onChange={handleChange} /></Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column><Input fluid placeholder='Phone Number' name="phoneNumber" defaultValue={values.phoneNumber} onChange={handleChange} /></Grid.Column>
                    <Grid.Column><Input fluid placeholder='Address Line 1' name="addressLine1" defaultValue={values.addressLine1} onChange={handleChange} /></Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column><Input fluid placeholder='Address Line 2' name="addressLine2" defaultValue={values.addressLine2} onChange={handleChange} /></Grid.Column>
                    <Grid.Column><Input fluid placeholder='Postcode' name="postcode" defaultValue={values.postcode} onChange={handleChange} /></Grid.Column>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column><Button fluid onClick={handleGetState}>GetState</Button></Grid.Column>
                    <Grid.Column><Button fluid disabled>Back</Button></Grid.Column>
                    <Grid.Column><Button fluid onClick={this.continue}>Continue</Button></Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
 
export default FormUserDetails;