import React, { Component } from 'react';
import { Grid, Button } from 'semantic-ui-react';

class FormProductOptions extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    getProducts = (values) => {
        if (values.productOne.selected === true) {
            return (
                <span>Product One</span>
            );
        } else  {
            console.log('false');
        }
    }

    render() {
        const { values, handleChange, handleGetState } = this.props;
        return (
            <Grid>
                <Grid.Row>
                    <h3>You have selected {this.getProducts(this.props.values)}</h3>
                </Grid.Row>
                <Grid.Row columns={3}>
                    <Grid.Column><Button fluid onClick={handleGetState}>GetState</Button></Grid.Column>
                    <Grid.Column><Button fluid onClick={this.back}>Back</Button></Grid.Column>
                    <Grid.Column><Button fluid onClick={this.continue}>Continue</Button></Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
 
export default FormProductOptions;